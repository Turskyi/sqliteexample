package ua.turskyi.sqliteexample

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import ua.turskyi.sqlite.R
import ua.turskyi.sqliteexample.sqlite.CustomDatabaseHelper
import ua.turskyi.sqliteexample.sqlite.DBAppThread
import ua.turskyi.sqliteexample.sqlite.model.Profile

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private lateinit var database: CustomDatabaseHelper
    private lateinit var appThread: DBAppThread

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appThread =
            DBAppThread(threadName = "MainActivity")
        appThread.start()
        database = CustomDatabaseHelper(this)

        btnSendToSecondActivity.setOnClickListener {
            saveProfile()
            sendToSecondActivity()
        }

        btnShowData.setOnClickListener {
            val intent = Intent(this, ThirdActivity::class.java)
            startActivity(intent)
        }
    }

    private fun saveProfile() {
        val profile = Profile(
            "${editTextName.text}",
            "${editTextAge.text}"
        )
        appThread.postTask {
            database.insertData(profile)
        }
    }


    private fun sendToSecondActivity() {
        val intent = Intent(this, SecondActivity::class.java)
        this.startActivity(intent)
    }
}
