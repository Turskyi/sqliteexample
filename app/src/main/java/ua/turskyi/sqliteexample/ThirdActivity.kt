package ua.turskyi.sqliteexample

import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_third.*
import ua.turskyi.sqlite.R
import ua.turskyi.sqliteexample.sqlite.CustomDatabaseHelper
import ua.turskyi.sqliteexample.sqlite.model.Profile

class ThirdActivity : AppCompatActivity(R.layout.activity_third) {

    private lateinit var appThread: Handler
    private lateinit var profile: Profile
    private lateinit var database: CustomDatabaseHelper
    private val uiHandler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val handlerThread = HandlerThread("ThirdActivity")
        handlerThread.start()
        val looper: Looper = handlerThread.looper
        appThread = Handler(looper)

        database = CustomDatabaseHelper(this)

        val task = Runnable {
            val result = database.getData()
                profile = result
                showInfo()
        }

        appThread.post(task)
    }

    private fun showInfo() {
        uiHandler.post {
            name.text = profile.name
            age.text = profile.age
            language.text = profile.language
        }
    }
}