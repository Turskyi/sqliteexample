package ua.turskyi.sqliteexample.sqlite

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import ua.turskyi.sqliteexample.sqlite.model.Profile

class CustomDatabaseHelper(context: Context) : SQLiteOpenHelper(
    context, databaseName, null,
    databaseVersion
) {

    companion object {
        private const val databaseName: String = "AppDB"
        private const val databaseVersion: Int = 1

        //if change the table
//        private const val databaseVersion: Int = 2
    }

    private val profileTable = "CREATE TABLE ${Profile.TABLE_NAME}(" +
            "${Profile.COLUMN_ID} INTEGER PRIMARY KEY AUTOINCREMENT," +
            "${Profile.COLUMN_NAME} TEXT," +
            "${Profile.COLUMN_AGE} TEXT," +
            "${Profile.COLUMN_LANGUAGE} TEXT" +

            //if change the table
//            ",${Profile.COLUMN_COUNTRY} TEXT" +
            ");"

    //if change the table
//    private val migrationOneToTwo = "ALTER TABLE ${Profile.TABLE_NAME}" +
//            "ADD COLUMN ${Profile.COLUMN_COUNTRY} TEXT;"

    override fun onCreate(database: SQLiteDatabase?) {
        database?.execSQL(profileTable)
    }

    override fun onUpgrade(database: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
//        if (oldVersion == 1 && newVersion == 3){
//            database?.execSQL(migrationOneToTwo)
//            //TODO: exec migrationTwoToThree
//        } else if (oldVersion == 2 && newVersion == 3) {
////TODO: exec migrationTwoToThree
//        }
    }

    fun updateData(profile: Profile) {
        val contentValues = ContentValues()
        contentValues.put(Profile.COLUMN_NAME, profile.name)
        contentValues.put(Profile.COLUMN_AGE, profile.age)
        contentValues.put(Profile.COLUMN_LANGUAGE, profile.language)

        writableDatabase.update(
            Profile.TABLE_NAME, contentValues,
            "${Profile.COLUMN_ID} = ${profile.id}", null
        )
    }

    fun insertData(profile: Profile) {
        val contentValues = ContentValues()
        contentValues.put(Profile.COLUMN_NAME, profile.name)
        contentValues.put(Profile.COLUMN_AGE, profile.age)
        contentValues.put(Profile.COLUMN_LANGUAGE, profile.language)
        writableDatabase.insert(Profile.TABLE_NAME, null, contentValues)
    }

    fun getData(): Profile {
        val profile = Profile()
        val cursor = readableDatabase.query(
            Profile.TABLE_NAME, null, null,
            null, null, null, null
        )
        if (cursor.moveToFirst()) {
            val id = cursor.getColumnIndex(Profile.COLUMN_ID)
            val nameIndex = cursor.getColumnIndex(Profile.COLUMN_NAME)
            val ageIndex = cursor.getColumnIndex(Profile.COLUMN_AGE)
            val languageIndex = cursor.getColumnIndex(Profile.COLUMN_LANGUAGE)
            profile.id = cursor.getInt(id)
            profile.name = cursor.getString(nameIndex)
            profile.age = cursor.getString(ageIndex)
            profile.language = cursor.getString(languageIndex)
        }
        cursor.close()
        return profile
    }

    //if delete
    fun deleteProfile(profile: Profile) {
        writableDatabase.delete(
            Profile.TABLE_NAME,
            "${Profile.COLUMN_ID} = ${profile.id}", null
        )

        //another option
//            "${Profile.COLUMN_ID} = ?", arrayOf(profile.id.toString()))
    }

    fun deleteAll(): Boolean {
        return writableDatabase.delete(Profile.TABLE_NAME, null, null) > 0

        //another option
//        writableDatabase.execSQL("DELETE FROM ${Profile.TABLE_NAME}")


    }
}