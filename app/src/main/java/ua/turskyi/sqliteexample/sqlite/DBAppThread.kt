package ua.turskyi.sqliteexample.sqlite

import android.os.Handler
import android.os.HandlerThread
import java.lang.Exception

class DBAppThread(threadName: String) : HandlerThread(threadName) {
    private lateinit var personalHandler: Handler

    override fun onLooperPrepared() {
        super.onLooperPrepared()

        personalHandler = Handler(looper)
    }

    fun postTask(task: Runnable) {
        try {
            personalHandler.post(task)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}