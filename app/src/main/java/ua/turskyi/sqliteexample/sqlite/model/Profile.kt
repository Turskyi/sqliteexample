package ua.turskyi.sqliteexample.sqlite.model

data class Profile(
    var id: Int,
    var name: String?,
    var age: String?,
    var language: String?
) {
    constructor(name: String?, age: String?) : this(0, name, age, null)
    constructor() : this(0, null, null, null)

    companion object {
        const val TABLE_NAME = "profile"
        const val COLUMN_ID = "id"
        const val COLUMN_NAME = "name"
        const val COLUMN_AGE = "age"
        const val COLUMN_LANGUAGE = "language"

        //if change the table
//        const val COLUMN_COUNTRY = "language"
    }
}