package ua.turskyi.sqliteexample

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_second.*
import ua.turskyi.sqlite.R
import ua.turskyi.sqliteexample.sqlite.CustomDatabaseHelper
import ua.turskyi.sqliteexample.sqlite.model.Profile

class SecondActivity : AppCompatActivity(R.layout.activity_second) {

    private lateinit var appThread: Handler
    private lateinit var profile: Profile
    private lateinit var database: CustomDatabaseHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val handlerThread = HandlerThread("SecondActivity")
        handlerThread.start()
        val looper: Looper = handlerThread.looper
        appThread = Handler(looper)

        database = CustomDatabaseHelper(this)

        getLocalData()

        btnSendToThirdActivity.setOnClickListener {
            saveProfile()
            sendToThirdActivity()
        }
    }

    private fun getLocalData() {
        val task = Runnable {
            val result = database.getData()
            profile = result
        }
        appThread.post(task)
    }

    private fun saveProfile() {
        profile.language = editTextLanguage.text.toString()
        appThread.post {
            database.updateData(profile)
        }
    }

    private fun sendToThirdActivity() {
        val intent = Intent(this, ThirdActivity::class.java)
        this.startActivity(intent)
    }

}